// @flow
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class App extends Component {
    componentWillMount() {
        // Initialize Firebase
        const config = {
            apiKey: "AIzaSyBFgqUg7on7mGZgVSelnSOya1-aAJvHF10",
            authDomain: "reactnative-f9747.firebaseapp.com",
            databaseURL: "https://reactnative-f9747.firebaseio.com",
            storageBucket: "reactnative-f9747.appspot.com",
            messagingSenderId: "551713718403"
        };
        firebase.initializeApp(config);
    }

    render() {
      const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <Provider store={store}>
              <Router/>
            </Provider>
        )
    }
}

export default App;
