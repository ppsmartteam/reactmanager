import React from 'react';
import {Scene, Router, Actions} from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import AddEmployee from './components/AddEmployee';

const RouterComponent = () => {
    return (
        <Router sceneStyle={{
            paddingTop: 65
        }}>
            <Scene key="auth">
                <Scene key="login" component={LoginForm} title="Please log in" initial/>
            </Scene>
            <Scene key="main">
                <Scene
                  key="employeeList"
                  component={EmployeeList}
                  title="Employees"
                  rightTitle="Add"
                  onRight={() => Actions.addEmployee()}
                />
                <Scene key="addEmployee"
                  title="Create Employee"
                  component={AddEmployee}>
                  </Scene>

            </Scene>
        </Router>
    )
}
export default RouterComponent;
