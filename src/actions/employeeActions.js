import firebase from 'firebase';
import {EMPLOYEE_UPDATE, EMPLOYEE_CREATE} from './types';
import {Actions} from 'react-native-router-flux';

export const employeeUpdate = ({prop, value}) => {
    return {
        type: EMPLOYEE_UPDATE,
        payload: {
            prop,
            value
        }
    }
}

export const addEmployee = ({name, phone, shift}) => {
    const {currentUser} = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/employees`)
        .push({name, phone, shift})
        .then(() => {
          dispatch({type: EMPLOYEE_CREATE}); //clear the form
          Actions.employeeList({type: 'reset'})//navigate back to list screen
        });
        //reset view stack so we don't have back button
    }
};
