import React, {Component} from 'react';
import {connect} from 'react-redux';
import {employeeUpdate, addEmployee} from '../actions';
import {Picker, Text} from 'react-native';
import {
  Card,
  CardSection,
  Input,
  Button
} from './common';


class AddEmployee extends Component {
  onButtonPress() {
    const {name, phone, shift} = this.props;
    this.props.addEmployee({name, phone, shift});
  }

  render() {
    return (
      <Card>
        <CardSection>
          <Input
            label="Name"
            placeholder="Jane"
            value={this.props.name}
            onChangeText={text => this.props.employeeUpdate({prop: 'name', value: text})}/>
        </CardSection>

        <CardSection>
          <Input
            label="Phone"
            placeholder="088-888-8888"
            value={this.props.phone}
            onChangeText={text => this.props.employeeUpdate({prop: 'phone', value: text})}/>
        </CardSection>

        <CardSection style={{flexDirection: 'column'}}>
          <Text style={styles.pickerTextStyle}>
            Shift
          </Text>
          <Picker
            style={{flex:1}}
            selectedValue={this.props.shift}
            onValueChange={value => this.props.employeeUpdate({prop: 'shift', value})}>
            <Picker.item label="Monday" value="Monday" />
            <Picker.item label="Tuesday" value="Tuesday" />
            <Picker.item label="Wednesday" value="Wednesday" />
            <Picker.item label="Thursday" value="Thursday" />
            <Picker.item label="Friday" value="Friday" />
            <Picker.item label="Saturday" value="Saturday" />
            <Picker.item label="Sunday" value="Sunday" />

          </Picker>
        </CardSection>

        <CardSection>
          <Button onPress={this.onButtonPress.bind(this)}>Save</Button>
        </CardSection>
      </Card>
    )
  }
}

const styles = {
  pickerTextStyle: {
    fontSize: 18,
    paddingLeft: 20
  }
}

const mapStateToProps = (state) => {
  const {name, phone, shift} = state.employee;
  return {
    name,
    phone,
    shift
  }
}

export default connect(mapStateToProps, {
  employeeUpdate,
  addEmployee})(AddEmployee);
