import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Card, CardSection, Input, Button, Spinner} from './common';
import {connect} from 'react-redux';
import {emailChanged, passwordChanged, loginUser} from '../actions';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmitButtonPressed = this.handleSubmitButtonPressed.bind(this);
    }

    handleEmailChange(text) {
        this.props.emailChanged(text);
        console.log(this.props.email);
    }

    handlePasswordChange(password) {
        this.props.passwordChanged(password);
        console.log(this.props.password);
    }

    handleSubmitButtonPressed() {
        const {email, password} = this.props;
        this.props.loginUser({email, password});
        console.log(`EMAIL: ${email} with PSWD: ${password}`)
    }

    renderButton() {
      if(this.props.loading) {
        return <Spinner size="large"/>;
      }

      return (
        <Button onPress={this.handleSubmitButtonPressed}>
          Login
        </Button>
      );
    }
    
    render() {
        console.log(this.props.error);
        return (
            <Card>
              <CardSection>
                <Input label="Email" placeholder="email.email.com" onChangeText={this.handleEmailChange} value={this.props.email}/>
              </CardSection>

              <CardSection>
                <Input secureTextEntry label="Password" placeholder="password" onChangeText={this.handlePasswordChange}/>
              </CardSection>

              <Text style={styles.errorTextStyle}>
                {this.props.error}
              </Text>

              <CardSection>
                {this.renderButton()}
              </CardSection>
            </Card>
        )
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
};

const mapStateToProps = (state) => {
    const {email, password, error, loading} = state.auth;
    return {email, password, error, loading}
};

export default connect(mapStateToProps, {emailChanged, passwordChanged, loginUser})(LoginForm);
