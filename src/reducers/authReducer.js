import {EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER_SUCCESS, LOGIN_USER_FAIL, LOGIN_USER_START} from '../actions/types';

const INITIAL_STATE = {
    email: '',
    password: '',
    loading: false,
    error: '',
    user: ''
}

export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
        case EMAIL_CHANGED:
            return {
                ...state,
                email: action.text
            };
        case PASSWORD_CHANGED:
            return {
                ...state,
                password: action.password
            }
        case LOGIN_USER_START:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.user,
                loading: false,
                email: '',
                password: ''
            }
        case LOGIN_USER_FAIL:
            return {
                ...state,
                error: 'Authentication Failed',
                password: '',
                loading: false
            }
        default:
            return state;
    }
}
